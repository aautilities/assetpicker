//
//  AssetImageCell.swift
//  GalleryQueueRef
//
//  Created by Allister Alambra on 10/26/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit
import Photos

public class AssetImageCell:UICollectionViewCell{
    
    var label:UILabel?
    var imageManager: PHImageManager?
    var assetImage:UIImageView!
    var assetCheckImage:UIImageView!
    var isAssetHidden:Bool = true
    var index:Int = -1
    var imageInfo:String?
    
    var imageAsset: PHAsset? {
        didSet {
            self.imageManager?.requestImageForAsset(imageAsset!, targetSize: CGSize(width: 320, height: 320), contentMode: .AspectFill, options: nil) { image, info in
                self.assetImage.image = image
                self.imageInfo = self.imageAsset?.localIdentifier
            }
        }
    }
    
}