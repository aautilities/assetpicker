//
//  GalleryQueue.swift
//  GalleryQueueRef
//
//  Created by Allister Alambra on 10/26/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit
import Photos
import CoreData

public class AssetPicker:UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, PHPhotoLibraryChangeObserver{
    
    /*
    * Object owner of UIView, expected to be UIViewController
    */
    var owner:UIViewController?
    
    var imageSizeFactor:CGFloat = 10.0
    
    var collectionView:UICollectionView!
    
    var images: PHFetchResult!
    
    var imageCacheController: ImageCacheController?
    
    var selectedURLS:[String] = []
    
    //MARK: Lifecycle Methods
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.owner?.respondsToSelector(Selector("edgesForExtendedLayout")) != nil){
            self.owner?.edgesForExtendedLayout = UIRectEdge.None
        }
        
        images = PHAsset.fetchAssetsWithMediaType(.Image, options: nil)
        imageCacheController = ImageCacheController(imageManager: AssetPickerManager.sharedInstance.imageManager!, images: images, preheatSize: 1)
        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: self.view.frame.width/imageSizeFactor, height: self.view.frame.width/imageSizeFactor)
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(AssetImageCell.self, forCellWithReuseIdentifier: "AssetCell")
        collectionView.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(collectionView)
        
        let doneBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("doneAction"))
        self.navigationItem.setRightBarButtonItem(doneBarButtonItem, animated: false)
        
    }
    
    //MARK: UICollectionView Overrides
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images!.count
    }
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellIdentifier = "AssetCell"
        var cell:AssetImageCell? = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as? AssetImageCell
        if(cell==nil){
            cell = AssetImageCell()
        }
        cell?.label = UILabel(frame: CGRectMake(0, 0, 50, 20))
        cell?.label?.textColor = UIColor.blackColor()
        cell?.addSubview((cell?.label!)!)
        if cell?.assetImage == nil{
            cell?.assetImage = UIImageView(frame: CGRectMake(0, 0, self.view.frame.width/imageSizeFactor, self.view.frame.width/imageSizeFactor))
        }
        cell?.imageManager = AssetPickerManager.sharedInstance.imageManager!
        cell?.imageAsset = self.images[indexPath.item] as? PHAsset
        cell?.addSubview((cell?.assetImage)!)
        cell?.assetCheckImage = UIImageView(frame: CGRectMake(0, 0, self.view.frame.width/15, self.view.frame.width/15))
        
        let bundle = NSBundle(forClass: AssetPickerManager.self)
        cell?.assetCheckImage.image = UIImage(named: "tick.png", inBundle: bundle, compatibleWithTraitCollection: nil)
        cell?.addSubview((cell?.assetCheckImage)!)
        cell?.assetCheckImage.hidden = (cell?.isAssetHidden)!
        return cell!
        
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = self.collectionView?.cellForItemAtIndexPath(indexPath) as! AssetImageCell
        cell.isAssetHidden = !cell.isAssetHidden
        cell.assetCheckImage.hidden = cell.isAssetHidden
    }
    
    //MARK: ScrollViewDelegate Overrides
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        let indexPaths = collectionView?.indexPathsForVisibleItems()
        imageCacheController!.updateVisibleCells(indexPaths as [NSIndexPath]!)
    }
    
    //MARK: PHPhotoLibraryChangeObserver
    public func photoLibraryDidChange(changeInstance: PHChange) {
        let changeDetails = changeInstance.changeDetailsForFetchResult(images!)
        
        self.images = changeDetails!.fetchResultAfterChanges
        dispatch_async(dispatch_get_main_queue()) {
            let indexPaths = self.collectionView?.indexPathsForVisibleItems()
            for indexPath in indexPaths as [NSIndexPath]! {
                if changeDetails!.changedIndexes!.containsIndex(indexPath.item) {
                    let cell = self.collectionView?.cellForItemAtIndexPath(indexPath) as! AssetImageCell
                    cell.imageAsset = changeDetails!.fetchResultAfterChanges[indexPath.item] as? PHAsset
                }
            }
        }
    }
    
    //MARK: Private Methods
    func doneAction(){
        
        selectedURLS.removeAll()
        
        for index:Int in 0..<images.count{
            let tIndex = NSIndexPath(forRow: index, inSection: 0)
            let cell = self.collectionView?.cellForItemAtIndexPath(tIndex) as! AssetImageCell
            if(!cell.isAssetHidden){
                selectedURLS.append(cell.imageInfo!)
                cell.isAssetHidden = true
                cell.assetCheckImage.hidden = cell.isAssetHidden
            }
        }
        
        savePickingSession()
        
        self.dismissViewControllerAnimated(false, completion: nil)
        
    }
    
    // Convert to Variadic Function
    func savePickingSession() -> Void{
        
        // Retrieve current working context
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all records first
        var reportCount:Int = 0
        if AssetPickerManager.sharedInstance.currentReport == nil{
            let fetchRequest = NSFetchRequest(entityName: "Report")
            do {
                let results = try managedContext.executeFetchRequest(fetchRequest)
                reportCount = results.count
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
            
            let reportEntity =  NSEntityDescription.entityForName("Report",
                inManagedObjectContext:managedContext)
            let report:Report = Report(entity: reportEntity!,
                insertIntoManagedObjectContext: managedContext)
            report.report_id = AssetPickerManager.sharedInstance.report_id
            report.surrogate_id = String(reportCount)
            AssetPickerManager.sharedInstance.currentReport = report
        }
        let entity =  NSEntityDescription.entityForName("ReportImage",
            inManagedObjectContext:managedContext)
        var imageTargets:[ReportImage] = []
        for info:String in self.selectedURLS{
            if !AssetPickerManager.sharedInstance.doesAssetExist(info){
                let reportImage:ReportImage = ReportImage(entity: entity!, insertIntoManagedObjectContext: managedContext)
                reportImage.image_url = info
                reportImage.image_report = AssetPickerManager.sharedInstance.currentReport
                imageTargets.append(reportImage)
            }
        }
        if AssetPickerManager.sharedInstance.currentReport == nil{
            AssetPickerManager.sharedInstance.currentReport!.report_image = NSMutableSet(array: imageTargets)
        }
        else{
            for imageTarget:ReportImage in imageTargets{
                AssetPickerManager.sharedInstance.currentReport!.report_image?.addObject(imageTarget)
            }
            
        }
        
        
        do {
            try managedContext.save()
            self.navigationController?.popViewControllerAnimated(true)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
}