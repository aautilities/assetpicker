//
//  CoreDataUtility.swift
//  GalleryQueueRef
//
//  Created by Allister Alambra on 10/26/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import AVFoundation
import Photos

public class AssetPickerManager{
    
    /*
    *   Singleton Instance
    */
    public static let sharedInstance = AssetPickerManager()
    
    public var assetPicker:AssetPicker?
    public var cameraPicker:CameraPicker?
    public var imageManager:PHCachingImageManager?
    public var currentReport:Report? = nil
    
    public var selectedImages:[ReportImage]?
    
    /*
    *   Default Instance String
    */
    public var report_id:String = "DF"
    
    //MARK: Initializer Functions
    init(){
        self.assetPicker = AssetPicker()
        self.cameraPicker = CameraPicker()
        self.imageManager = PHCachingImageManager()
    }
    
    // MARK: - Asset Picker Function
    public func showPicker(navigationController: UINavigationController, frame:CGRect){
        
        let actionSheetController = UIAlertController(title: "", message: "", preferredStyle: .ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            navigationController.dismissViewControllerAnimated(true, completion: nil)
        }
        actionSheetController.addAction(cancelAction)
        
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .Default) { action -> Void in
            navigationController.pushViewController(AssetPickerManager.sharedInstance.cameraPicker!, animated: true)
        }
        actionSheetController.addAction(takePictureAction)
        
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .Default) { action -> Void in
            navigationController.pushViewController(AssetPickerManager.sharedInstance.assetPicker!, animated: true)
        }
        actionSheetController.addAction(choosePictureAction)
        
        actionSheetController.popoverPresentationController!.sourceView = navigationController.visibleViewController!.view
        actionSheetController.popoverPresentationController!.sourceRect = frame
        navigationController.visibleViewController!.presentViewController(actionSheetController, animated: true, completion: nil)
        
    }
    
    // MARK: - Core Data Utilities
    public func getReports() -> [AnyObject]?{
        
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all reports
        let fetchRequest = NSFetchRequest(entityName: "Report")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            return results
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return []
        }
        
    }
    
    public func getReportImagesByUsingActiveSession() -> [ReportImage]{
        return AssetPickerManager.sharedInstance.getReportImagesByReportIdAsImages(AssetPickerManager.sharedInstance.report_id)!
    }
    
    public func getReportImagesBySurrogateIdAsImages(id:String) -> [ReportImage]?{
        
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all reports
        let fetchRequest = NSFetchRequest(entityName: "Report")
        fetchRequest.predicate = NSPredicate(format: "surrogate_id == %@", id)
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if results.count > 0{
                let imagesTemp = (results[0] as! Report).report_image
                var reportImages:[ReportImage] = []
                for tImage in imagesTemp!{
                    let tempReportImage:ReportImage = tImage as! ReportImage
                    
                    let fetchResult:PHFetchResult = PHAsset.fetchAssetsWithLocalIdentifiers([(tImage as! ReportImage).image_url!], options: nil)
                    let tAsset:PHAsset = (fetchResult[0] as? PHAsset)!
                    
                    AssetPickerManager.sharedInstance.imageManager!.requestImageForAsset(tAsset, targetSize: CGSize(width: 320, height: 320), contentMode: .AspectFill, options: nil) { image, info in
                        tempReportImage.assetImage = UIImageView()
                        tempReportImage.assetImage!.image = image
                        reportImages.append(tempReportImage)
                    }
                    
                    
                }
                return reportImages
            }
            return []
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return []
        }
        
    }
    
    public func getReportImagesByReportIdAsImages(id:String) -> [ReportImage]?{
        
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all reports
        let fetchRequest = NSFetchRequest(entityName: "Report")
        fetchRequest.predicate = NSPredicate(format: "report_id == %@", id)
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if results.count > 0{
                AssetPickerManager.sharedInstance.currentReport = results[0] as? Report
                let imagesTemp = AssetPickerManager.sharedInstance.currentReport!.report_image
                var reportImages:[ReportImage] = []
                for tImage in imagesTemp!{
                    let tempReportImage:ReportImage = tImage as! ReportImage
                    
                    let fetchResult:PHFetchResult = PHAsset.fetchAssetsWithLocalIdentifiers([(tImage as! ReportImage).image_url!], options: nil)
                    let tAsset:PHAsset = (fetchResult[0] as? PHAsset)!
                    
                    AssetPickerManager.sharedInstance.imageManager!.requestImageForAsset(tAsset, targetSize: CGSize(width: 320, height: 320), contentMode: .AspectFill, options: nil) { image, info in
                        tempReportImage.assetImage = UIImageView()
                        tempReportImage.assetImage!.image = image
                        reportImages.append(tempReportImage)
                    }
                    
                }
                return reportImages
            }
            return []
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return []
        }
        
    }
    
    public func getReportImagesBySurrogateId(id:String) -> [ReportImage]?{
        
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all reports
        let fetchRequest = NSFetchRequest(entityName: "Report")
        fetchRequest.predicate = NSPredicate(format: "surrogate_id == %@", id)
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if results.count > 0{
                let imagesTemp = (results[0] as! Report).report_image
                var images:[ReportImage] = []
                for tImage in imagesTemp!{
                    images.append(tImage as! ReportImage)
                }
                AssetPickerManager.sharedInstance.selectedImages = images
                return images
            }
            return []
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return []
        }
        
    }
    
    public func getReportImagesByReportId(id:String) -> [AnyObject]?{
        
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all reports
        let fetchRequest = NSFetchRequest(entityName: "Report")
        fetchRequest.predicate = NSPredicate(format: "report_id == %@", id)
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if results.count > 0{
                let imagesTemp = (results[0] as! Report).report_image
                var images:[AnyObject] = []
                for tImage in imagesTemp!{
                    images.append(tImage as! ReportImage)
                }
                return images
            }
            return []
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return []
        }
        
    }
    
    public func doesAssetExist(image_url:String) -> Bool{
        
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all reports
        let fetchRequest = NSFetchRequest(entityName: "ReportImage")
        fetchRequest.predicate = NSPredicate(format: "image_url == %@", image_url)
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if results.count > 0{
                return true
            }
            return false
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return false
        }
        
    }
    
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.seer.ph.MiniBlog" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let bundle = NSBundle(forClass: AssetPickerManager.self)
        let modelURL = bundle.URLForResource("GalleryQueueRef", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    public func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
}