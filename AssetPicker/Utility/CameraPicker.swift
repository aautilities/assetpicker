
//
//  CameraPicker.swift
//  Pods
//
//  Created by Allister Alambra on 10/30/15.
//
//

import Foundation
import UIKit
import Photos
import CoreData

public class CameraPicker: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var imagePicker: UIImagePickerController!
    var photo_url:String = ""
    
    override public func viewDidLoad() {
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        
        presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    
    public func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        UIImageWriteToSavedPhotosAlbum(image, self, Selector("image:didFinishSavingWithError:contextInfo:"), nil)
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    public func image(image:UIImage, didFinishSavingWithError:NSError, contextInfo:UnsafePointer<Void>){
        self.loadLastPhoto()
        self.savePickingSession()
    }
    
    func loadLastPhoto() {
        
        // Sort the images by creation date
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        if let fetchResult:PHFetchResult? = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: fetchOptions) {
            
            // If the fetch result isn't empty,
            // proceed with the image request
            if fetchResult!.count > 0 {
                // Perform the image request
                let targetAsset:PHAsset = fetchResult!.objectAtIndex(fetchResult!.count - 1) as! PHAsset
                self.photo_url = targetAsset.localIdentifier
            }
        }
    }
    
    // Convert to Variadic Function
    func savePickingSession() -> Void{
        
        // Retrieve current working context
        let assetManager = AssetPickerManager.sharedInstance
        let managedContext = assetManager.managedObjectContext
        
        // Retrieve all records first
        
        if AssetPickerManager.sharedInstance.currentReport == nil{
            
            var reportCount:Int = 0
            let fetchRequest = NSFetchRequest(entityName: "Report")
            do {
                let results = try managedContext.executeFetchRequest(fetchRequest)
                reportCount = results.count
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
            
            let reportEntity =  NSEntityDescription.entityForName("Report",
                inManagedObjectContext:managedContext)
            let report:Report = Report(entity: reportEntity!,
                insertIntoManagedObjectContext: managedContext)
            report.report_id = AssetPickerManager.sharedInstance.report_id
            report.surrogate_id = String(reportCount)
            AssetPickerManager.sharedInstance.currentReport = report
        }
        
        let entity =  NSEntityDescription.entityForName("ReportImage",
            inManagedObjectContext:managedContext)
        var imageTargets:[ReportImage] = []
        let reportImage:ReportImage = ReportImage(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        reportImage.image_url = self.photo_url
        reportImage.image_report = AssetPickerManager.sharedInstance.currentReport
        imageTargets.append(reportImage)
        
        if AssetPickerManager.sharedInstance.currentReport == nil{
            AssetPickerManager.sharedInstance.currentReport!.report_image = NSMutableSet(array: imageTargets)
        }
        else{
            for imageTarget:ReportImage in imageTargets{
                AssetPickerManager.sharedInstance.currentReport!.report_image?.addObject(imageTarget)
            }
        }
        
        do {
            try managedContext.save()
            self.navigationController?.popViewControllerAnimated(true)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
}