//
//  AssetPicker.h
//  AssetPicker
//
//  Created by Allister Alambra on 10/27/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AssetPicker.
FOUNDATION_EXPORT double AssetPickerVersionNumber;

//! Project version string for AssetPicker.
FOUNDATION_EXPORT const unsigned char AssetPickerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AssetPicker/PublicHeader.h>


