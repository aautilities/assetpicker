//
//  Report+CoreDataProperties.swift
//  GalleryQueueRef
//
//  Created by Allister Alambra on 10/27/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

public extension Report {
    
    @NSManaged var report_id: String?
    @NSManaged var surrogate_id: String?
    @NSManaged var report_image: NSMutableSet?
    
}
