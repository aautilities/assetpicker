//
//  ReportImage.swift
//  GalleryQueueRef
//
//  Created by Allister Alambra on 10/27/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//

import Foundation
import CoreData
import Photos

public class ReportImage: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    public var assetImage:UIImageView?
    
    public var imageManager: PHImageManager?
    
}
