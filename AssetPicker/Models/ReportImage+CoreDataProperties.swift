//
//  ReportImage+CoreDataProperties.swift
//  GalleryQueueRef
//
//  Created by Allister Alambra on 10/27/15.
//  Copyright © 2015 Seer Technologies, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

public extension ReportImage {
    
    @NSManaged var image_url: String?
    @NSManaged var id: String?
    @NSManaged var image_report: Report?
    @NSManaged var remarks: String?
    
}
